# Overview
This directory contains demos of traditional unet, improved unet in jupyter notebooks using multiple datasets

## Caution!!!
Gitlab jupyter notebook renderer `fails show multiple outputs at each cell`. Outputs include just print outputs and matplotlib figure outputs. In such a case it is advised to download the jupyter notebook and view it.

## Directory Description
- `model_files` - This directory contains tensorflow model files of UNET networks

## Notebook Description
- `unet-salt-identification-challenge.ipynb` - this jupyter notebook shows of demo of traditional unet using salt_identification data
- `improved-unet-salt-identification-challenge` - this jupyter notebook shows of demo of improved unet using salt_identification data
