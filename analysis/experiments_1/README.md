# Experiment In Progress

## Directory Description
This directory contains jupyter notebooks that collectively perform experiments to show performance difference bet traditional U-Net & Improved U-Net. View the `analysis_of_results.ipynb` to view the results collected so far.

## Caution!!!
Gitlab jupyter notebook renderer `fails show multiple outputs at each cell`. Outputs include just print outputs and matplotlib figure outputs. In such a case it is advised to download the jupyter notebook and view it.

## File Description
- `config.py` - python file containing configuration functions.
- `analysis_of_results.ipynb` - jupyter notebook which analyzes results collected so far.
- `hyperparameter_generator.ipynb` - jupyter notebook which generates hyperparameters of unets and improved-unets for different datasets
- `unet-salt-identification-challenge.ipynb` - jupyter notebook which trains multiple unets having different hyperparameter settings using salt identification data.
- `improved-unet-salt-identification-challenge.ipynb` - jupyter notebook which trains multiple improved-unets having different hyperparameter settings using salt identification data.
