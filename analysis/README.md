# Description
This directory contains jupyter notebooks to demonstrate performance of experiments using improved unet.

## Caution!!!
Gitlab jupyter notebook renderer fails show multiple outputs at each cell. Outputs include just print outputs and matplotlib figure outputs. In such a case it is advised to download the jupyter notebook and view it.

# Directory description
- `demo` - This directory contains demos of traditional U-Net and improved U-Net in jupyter notebooks using multiple datasets.
- `experiments_(*)` - directories containing jupyter notebooks that collectively train multiples models to find or prove a hypothesis. For example, experiments to find the best configuration of U-Net and Improved U-Net.
