# IMPROVED UNET ARCHITECTURE FOR DENSE PREDICTION

## Overview
This is the project repo of R&D of a `CNN architecture` for `improved dense prediction`. **The architecture consists of multiple networks in top down hierarchical fashion, where the lower networks act as slaves and assists their respective master networks above**.

Prof. Geoffrey E. Hinton said, “Read enough to develop your intuition, then trust your intuition”. Prof Andrew Ng said we should implement and test ideas in deep learning even if our peers disagrees. It is very difficult to mathematically prove whether an architectural and algorithmic ideas in deep learning would work. From the aforementioned statements made by the great AI scientists it is clear that an easy way to make contribution to the deep learning research is to read enough literature to develop mathematical intuition, and use that intuition to generate ideas and then implement and test whether those ideas betters performance. I am taking that advise in this research project.

## Architecture Details
In a neural network the last layer (in U-Net the last transposed convolutional layer) sets the reference for the lower layers in terms of complexity of features these lower layers can compute. This is like a sanction; the deeper layers must compute more complex features than its respective lower layers. My intuition is that this restricts layers in neural network to computing more advanced features than they possibly can. This is like a football/soccer coach instructing that only his lone center forward can score goals and no other outfield players can; or say that his midfielders cannot advance into final 3rd of pitch; or saying that his full backs cannot advance to the opposition half. But this would be a suicidal tactics. We know in modern football all outfield players are given the freedom to advance forward, assist center forwards and even score goals. This also sets a higher benchmark for the lone center forward, for example if the second striker score 20 goals per season then this would spur the lone center forward to score may be 25 goals per season.

Likewise in the deep learning world the layers below last layer should be given enough freedom to express themselves and computes the most complex features they possibly can. The CNN architecture, which is an improved U-Net network, is a manifestation of that idea. In this network the lower transposed convolutional layers are given enough freedom to compute more complex features and assist their respective layers above. So this architecture consists of multiple neural networks embedded in it in a top down hierarchical fashion, where the lower networks (networks with less transposed convolutional layers) act as slaves and assists their respective master networks (networks with more transposed convolutional layers) above. This idea can also be transferred to other types of CNN architectures in classification, object detection, and object localization.

## Results so far
So far I have established that the improved U-Net architecture has much **faster and more stable convergence** than traditional U-Net architecture. Given the Neural Network is sufficiently complex and network has sufficient number of convolutional and transposed convolutional layers (for example for the salt identification dataset the there should be at least 4 convolutional and transposed convolutional layers) the **improved U-Net beats equivalent traditional U-Net all day on performance in terms of IoU and cost**. I hope in the next 1-2 months I will carry out enough research to make an airtight conclusion and make a publication.

You can check the class modules of U-Net(`unet.py`) and improved U-Net(`improved_unet.py`) in python files in in the following [directory](https://gitlab.com/itratrahman/improved_unet/tree/master/models).

You can check the demos of performance of traditional U-Net(`unet-salt-identification-challenge.ipynb`) and improved U-Net(`improved-unet-salt-identification-challenge.ipynb`) in the jupyter notebooks in the following [directory](https://gitlab.com/itratrahman/improved_unet/tree/master/analysis/demo).

## Caution!!!
Gitlab jupyter notebook renderer `fails show multiple outputs at each cell`. Outputs include just print outputs and matplotlib figure outputs. In such a case it is advised to download the jupyter notebook and view it.

## License
This repository has MIT License.

## Authors of this code repository
This repository is exclusively maintained and developed by `ITRAT RAHMAN` (CEO, Learners & Yearners).

## File Description
- `analysis` - contains experimental jupyter notebooks to demonstrate performance of traditional U-Net and improved U-Net models using different segmentation datasets.
- `dataset` - contains datasets to test the performance of image segmentation.
- `models` - this directory contains the class modules of U-Net and improved U-Net.

## Installation of Drivers & Softwares
The U-Net and improved U-Net models are trained using `gpu enabled virtual machines` on Google Cloud with the latest versions of [Anaconda](https://www.anaconda.com/download/) and `cuda drivers`installed. So do not try to run them without gpu-enabled workstations or linux kernels. I am currently using deep learning virtual machines which can be launched using a single command on Google Cloud ([More Information](https://cloud.google.com/deep-learning-vm/docs/tensorflow_start_instance)).
