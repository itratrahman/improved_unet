import time
import math
import random
import pandas as pd
import numpy as np
import tensorflow as tf
import progressbar
from .util import resize_image, binirize_mask, rle_encode_multimage, compute_iou_np

class improved_unet_1_1(object):

    '''
    Class module of improved U-Net
    '''

    def __init__(self, IMG_HEIGHT, IMG_WIDTH, IMG_CHANNELS,IMG_HEIGHT_RESIZE,IMG_WIDTH_RESIZE,MASK_CHANNELS,
                 conv_filters, conv_transpose_filters, kernel_sizes, strides,
                 max_pool_kernel_size, max_pool_strides, learning_rate, device, tf_seed, np_seed):

        '''
        Class constructors which initializes the hyperparameters of U-Net
        '''
        # hyperparameters of the unet
        self.IMG_HEIGHT = IMG_HEIGHT
        self.IMG_WIDTH = IMG_WIDTH
        self.IMG_CHANNELS = IMG_CHANNELS
        self.IMG_HEIGHT_RESIZE = IMG_HEIGHT_RESIZE
        self.IMG_WIDTH_RESIZE = IMG_WIDTH_RESIZE
        self.MASK_CHANNELS = MASK_CHANNELS
        self.conv_filters = conv_filters
        self.conv_transpose_filters = conv_transpose_filters
        self.kernel_sizes = kernel_sizes
        self.strides = strides
        self.max_pool_kernel_size = max_pool_kernel_size
        self.max_pool_strides = max_pool_strides
        self.learning_rate = learning_rate
        self.device = device
        self.tf_seed = tf_seed
        self.np_seed = np_seed

        # placeholders & operations & other variables of the U-Net
        self.X = None
        self.Ys = None
        self.Y = None
        self.Y_s = None
        self.Y_ = None
        self.cond = None
        self.training = None
        self.iou = None
        self.costs = None
        self.train_steps = None
        self.list_of_var_lists = None
        self.N_nets = None

        self.X, self.Ys, self.Y, self.Y_s, self.Y_, self.cond,\
        self.training, self.iou, self.costs, self.train_steps,\
        self.list_of_var_lists = self.create_model()

        # training parameters
        self.saver = None
        self.train_costs = None
        self.valid_costs = None
        self.best_cost = None
        self.best_iteration = None
        self.log_of_computation_time = []
        self.log_of_elapsed_time = []


    def create_cost_train_step(self, Y, deconv, var_list, optimizer, update_ops):

        with tf.device(self.device):
            # sigmoid of the last deconvolutional layer
            Y_ = tf.nn.sigmoid(deconv)
            # flatten the mask
            flat_y = tf.layers.Flatten()(Y)
            # flatten the predicted mask
            flat_y_ = tf.layers.Flatten()(Y_)

        with tf.device(self.device):
            # sigmoid_cross_entropy
            cost = tf.losses.sigmoid_cross_entropy(multi_class_labels=flat_y, logits=flat_y_)

        with tf.control_dependencies(update_ops):
            with tf.device(self.device):
                # train step
                train_step = optimizer.minimize(cost, var_list=var_list)

        return Y_, cost, train_step


    def intersection_of_union(self, label, pred):
        '''
        A function to compute iou in tensorflow
        '''
        return tf.py_func(compute_iou_np, [label, pred > 0.5], tf.float64)


    def create_model(self):

        '''
        A function to create improved UNET network
        '''
        cond = tf.placeholder(tf.int32, shape=(2,))
        # placeholder of input image
        X = tf.placeholder(tf.float32, [None, self.IMG_HEIGHT, self.IMG_WIDTH, self.IMG_CHANNELS])
        # resize input image
        if self.IMG_HEIGHT != self.IMG_HEIGHT_RESIZE or self.IMG_WIDTH != self.IMG_WIDTH_RESIZE:
            X_RESIZED = tf.image.resize_images(X, size=[self.IMG_HEIGHT_RESIZE, self.IMG_WIDTH_RESIZE])
        else:
            X_RESIZED = X
        # randomly flip up & down, left & right
        X_RESIZED = tf.cond(cond[0] > 0, lambda: tf.image.flip_up_down(X_RESIZED), lambda: X_RESIZED)
        X_RESIZED = tf.cond(cond[1] > 0, lambda: tf.image.flip_left_right(X_RESIZED), lambda: X_RESIZED)
        # placeholder of mask
        Y = tf.placeholder(tf.float32, [None, self.IMG_HEIGHT, self.IMG_WIDTH, self.MASK_CHANNELS])
        # resize output mask
        if self.IMG_HEIGHT != self.IMG_HEIGHT_RESIZE or self.IMG_WIDTH != self.IMG_WIDTH_RESIZE:
            Y_RESIZED = tf.image.resize_images(Y, size=[self.IMG_HEIGHT_RESIZE, self.IMG_WIDTH_RESIZE])
        else:
            Y_RESIZED = Y
        # randomly flip up & down, left & right
        Y_RESIZED = tf.cond(cond[0] > 0, lambda: tf.image.flip_up_down(Y_RESIZED), lambda: Y_RESIZED)
        Y_RESIZED = tf.cond(cond[1] > 0, lambda: tf.image.flip_left_right(Y_RESIZED), lambda: Y_RESIZED)
        # placeholder for mode of batch normalization
        training = tf.placeholder(tf.bool)
        # a list to store the convolutional layers
        convs = []
        # a list to store the deconvolutional layers
        deconvs = []
        # a list to store the Ys
        Ys = []
        # a list to store all the Y_s
        Y_s = []
        # a list to store the trainable variables of the networks
        list_of_var_lists = []
        # a list to store all the costs
        costs = []
        # a list to store all the train steps
        train_steps = []

        for n in range(len(self.kernel_sizes)):
            if n == 0:
                Ys.append(Y_RESIZED)
            else:
                Ys.append(tf.image.resize_images(Y_RESIZED, [self.IMG_HEIGHT_RESIZE//(2**n),self.IMG_WIDTH_RESIZE//(2**n)]))
        Ys = Ys[::-1]

        # compute number of nets
        self.N_nets = len(Ys)

        print("########################Layers of UNET########################")

        # create convolutational layers of UNET using for loop
        for n, conv_filter in enumerate(self.conv_filters):
            # if it is the 1st convolutional layer
            if n == 0:
                with tf.device(self.device):
                    with tf.variable_scope("Conv"+str(n)):
                        # conv layer
                        conv = tf.layers.conv2d(X_RESIZED, filters=conv_filter, kernel_size=self.kernel_sizes[n],
                                                strides=[self.strides,self.strides],
                                                kernel_initializer=tf.variance_scaling_initializer(),padding="SAME")
                        print(conv)
                        # activation layer
                        conv = tf.nn.elu(conv)
                        print(conv)
                        # append the layer to the designated list
                        convs.append(conv)
                        # batch normalization layer
                        conv = tf.layers.batch_normalization(conv, training=training)
                        print(conv)
                        # max pooling layer
                        conv = tf.layers.max_pooling2d(conv, self.max_pool_kernel_size, self.max_pool_strides, padding='SAME')
                        print(conv)
            # for rest of the layers
            else:
                with tf.device(self.device):
                    with tf.variable_scope("Conv"+str(n)):
                        # conv layer
                        conv = tf.layers.conv2d(conv, filters=conv_filter, kernel_size=self.kernel_sizes[n],
                                                strides=[self.strides,self.strides],
                                                kernel_initializer=tf.variance_scaling_initializer(),padding="SAME")
                        print(conv)
                        # activation layer
                        conv = tf.nn.elu(conv)
                        print(conv)
                        # append the layer to the designated list
                        convs.append(conv)
                        # batch normalization layer
                        conv = tf.layers.batch_normalization(conv, training=training)
                        print(conv)
                        # max pooling layer
                        conv = tf.layers.max_pooling2d(conv, self.max_pool_kernel_size, self.max_pool_strides, padding='SAME')
                        print(conv)

        # create deconvolutational layers of UNET using for loop
        for n, conv_transpose_filter in enumerate(self.conv_transpose_filters):
            # if it is not the last layer
            if n != len(self.conv_transpose_filters)-1:
                with tf.variable_scope("Deconv"+str(n)):
                        # deconvolutional layer
                        conv = tf.layers.conv2d_transpose(conv, filters=conv_transpose_filter,
                                                          kernel_size=self.kernel_sizes[::-1][n],
                                                          kernel_initializer=tf.variance_scaling_initializer(),
                                                          strides=[2,2], padding="SAME")
                        print(conv)

                with tf.variable_scope("Extn"+str(n)):
                        # take output of the deconvolutional layer and append it to the designated list
                        deconv = tf.layers.conv2d(conv, filters=1,kernel_size=self.kernel_sizes[::-1][n],
                                                  strides=[self.strides,self.strides],
                                                  kernel_initializer=tf.variance_scaling_initializer(),
                                                  padding="SAME")
                        deconvs.append(deconv)
                        print(deconv)

                with tf.variable_scope("Deconv"+str(n)):
                        # activation layer
                        conv = tf.nn.elu(conv)
                        print(conv)
                        # concat the corresponding convolutional layer (this improves accuracy)
                        conv = tf.concat((conv, convs[::-1][n]), axis=3)
                        print(conv)
                        # batch normalization layer
                        conv = tf.layers.batch_normalization(conv, training=training)
                        print(conv)

                var_list = tf.trainable_variables()
                var_list = [var for var in var_list if any(word in var.name for word in ['Deconv', "Conv", "Extn"+str(n)])]
                list_of_var_lists.append(var_list)

            # if it is the last layer
            else:
                with tf.device(self.device):
                    with tf.variable_scope("Deconv"+str(n)):
                        # deconvolutional layer
                        conv = tf.layers.conv2d_transpose(conv, filters=conv_transpose_filter,
                                                          kernel_size=self.kernel_sizes[::-1][n],
                                                          kernel_initializer=tf.variance_scaling_initializer(),
                                                          strides=[2,2], padding="SAME")
                        deconvs.append(conv)
                        print(conv)

                var_list = tf.trainable_variables()
                var_list = [var for var in var_list if any(word in var.name for word in ['Deconv', "Conv"])]
                list_of_var_lists.append(var_list)

        print("#############################################################")

        # optimizer
        optimizer = tf.train.AdamOptimizer(learning_rate=self.learning_rate)
        # update operation
        update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

        for y,deconv,var_list in zip(Ys, deconvs, list_of_var_lists):
            y_, cost, train_step = self.create_cost_train_step(y, deconv, var_list, optimizer, update_ops)
            Y_s.append(y_)
            costs.append(cost)
            train_steps.append(train_step)

        Y__ORIGINAL = Y_s[-1]

        # resize predicted mask to size of original image
        if self.IMG_HEIGHT != self.IMG_HEIGHT_RESIZE or self.IMG_WIDTH != self.IMG_WIDTH_RESIZE:
            Y_ = tf.image.resize_images(Y__ORIGINAL, size=[self.IMG_HEIGHT, self.IMG_WIDTH])
        else:
            Y_ = Y__ORIGINAL

        # rounding predicted mask
        Y_ = tf.round(Y_)
        # compute iou
        iou = self.intersection_of_union(Y,Y_)

        return X, Ys, Y, Y_s, Y_, cond, training, iou, costs, train_steps, list_of_var_lists


    def train_model(self, max_iter, batch_size,
                    train_indices, validation_indices, train_ids,
                    model_dir, model_name, generate_batch,*args):

        '''
        A function to traini UNET network
        '''
        # create feed dict for validation
        image_validation, mask_validation =  generate_batch(validation_indices,*args,train_set=True)
        feed_dict_validation =  {self.X: image_validation.astype(np.float32), self.Y: mask_validation.astype(np.float32),
                                 self.training: False, self.cond: np.array([0,0])}

        # start time
        start_time = time.time()
        # a variable to store the running residual time
        residual_time = 0
        # create a saver object
        saver = tf.train.Saver(max_to_keep=1)
        # lists to store the train cost, validation cost
        train_costs = [[] for n in range(self.N_nets)]
        valid_costs = [[] for n in range(self.N_nets)]
        # variables to store the best cost and best iteration
        best_cost = 100000
        best_iteration = None
        best_time = None
        # number of training data
        N = len(train_indices)
        # number of complete batches
        num_complete_mini_batches = math.floor(N/batch_size)
        # path of model saver object
        model_path = model_dir+model_name

        # create a graph session and optimize the network under it
        with tf.Session() as sess:

            # set random seed of tensorflow
            tf.set_random_seed(self.tf_seed)
            # set random seed of numpy
            np.random.seed(self.np_seed)
            # initialize global variables
            sess.run(tf.global_variables_initializer())

            # create an object of progress bar
            bar = progressbar.ProgressBar(maxval=max_iter, widgets=[
                '[',progressbar.Percentage(),']',
                progressbar.Bar(),
                '(',progressbar.DynamicMessage("train_cost"),')',
                '(',progressbar.DynamicMessage("valid_cost"),')',
                '(',progressbar.DynamicMessage("best_valid_cost"),')',
                '(',progressbar.DynamicMessage("time_elapsed"),'mins)',
                '(',progressbar.DynamicMessage("computation_time"),'mins)'
            ])
            # start the progress bar
            bar.start()
            # counter variable to track the number of iterations
            counter = 0
            # iterate until maximum number of iterations is reached
            while True:
                # break out of the inner for loop if maximum number of iterations is reached
                if counter>=max_iter:
                    break
                # iterate through the mini batches
                for k in range(0, num_complete_mini_batches+1):
                    # if the mini batch is a complete batch
                    if k < num_complete_mini_batches:
                        batch_indices = train_indices[list(range(k * batch_size, k * batch_size + batch_size))]
                    # handling the end case when the last mini batch is less than the batch_size
                    elif N % batch_size != 0:
                        batch_indices = train_indices[list(range(num_complete_mini_batches * batch_size, N))]

                    # conditions for flip up & down and left & right
                    conditions = np.random.choice([0,1], size=2)
                    # generate images and masks for the batch
                    image_batch, mask_batch = generate_batch(batch_indices,*args,train_set=True)

                    # feed dict of the batch
                    feed_dict_batch =  {self.X: image_batch.astype(np.float32),
                                        self.Y: mask_batch.astype(np.float32),
                                        self.training: True, self.cond: conditions}

                    # execute train steps & compute train and valid cost of all nets
                    for n in range(self.N_nets):
                        # execute optimization step
                        sess.run(self.train_steps[n], feed_dict=feed_dict_batch)
                        # start of residual time
                        if n < (self.N_nets-1):
                            residual_time_start = time.time()
                        # calculate temporary train cost and append it to the designated list
                        temp_train_cost = self.costs[n].eval(session=sess, feed_dict=feed_dict_batch)
                        train_costs[n].append(temp_train_cost)
                        # calculate temporary validation cost and append it to the designated list
                        temp_validation_cost = self.costs[n].eval(session=sess, feed_dict=feed_dict_validation)
                        valid_costs[n].append(temp_validation_cost)
                        # accumulate residual time
                        if n < (self.N_nets-1):
                            residual_time += (time.time()-residual_time_start)/60

                    # calculate running time from start point
                    time_elapsed = (time.time()-start_time)/60
                    # calculate computation time (barring the residual time
                    # needed to compute cost of slave network)
                    computation_time = time_elapsed-residual_time
                    # append the time_elapsed and computation_time to the designated log lists
                    self.log_of_elapsed_time.append(time_elapsed)
                    self.log_of_computation_time.append(computation_time)

                    temp_train_cost = [train_cost[-1] for train_cost in train_costs]
                    temp_validation_cost = [valid_cost[-1] for valid_cost in valid_costs]

                    # if valid cost is better than best recorded so far then
                    # update the parameters of the best model and save the model
                    if valid_costs[self.N_nets-1][-1] < best_cost:
                        best_time = computation_time
                        best_cost = valid_costs[self.N_nets-1][-1]
                        best_iteration = counter+1
                        saver.save(sess, model_path, global_step = best_iteration)

                    # update the progress bar
                    bar.update(counter+1, train_cost = temp_train_cost[-1], valid_cost = temp_validation_cost[-1],
                               best_valid_cost = best_cost, time_elapsed = time_elapsed, computation_time = computation_time)
                    # increment counter variable
                    counter += 1
                    # break out of the inner for loop if maximum number of iterations is reached
                    if counter>=max_iter:
                        break
            # finish the progress bar
            bar.finish()

        self.saver = saver
        self.train_costs = train_costs
        self.valid_costs = valid_costs
        self.best_cost = best_cost
        self.best_iteration = best_iteration
        self.best_time = best_time


    def compute_predictions(self, model_path, n_folds,test_ids, TEST_IMG_DIR,
                            threshold, submission_dir, submission_file,
                            generate_batch, HEIGHT, WIDTH, *args):

        '''
        A function to create predictions and submission file
        '''
        submission_path = submission_dir + submission_file

        with tf.Session() as sess:

            # set random seed of tensorflow
            tf.set_random_seed(self.tf_seed)
            # restore the best model
            self.saver.restore(sess, model_path)
            # number of test samples
            n = len(test_ids)
            # step size of each fold
            step = n//n_folds
            # a list to store the encoded pixels
            EncodedPixels = []
            # create a progress bar object
            bar = progressbar.ProgressBar(maxval=n_folds, widgets=[
                            '[',progressbar.Percentage(),']',
                            progressbar.Bar(),
                            '(',progressbar.DynamicMessage("number_of_folds_completed"),')'
                        ])
            # start progress bat
            bar.start()

            # iterate through each fold
            for i in range(n_folds):
                # start and end indices of the fold
                if i != (n_folds-1):
                    start = (step*i)
                    end = (step*(i+1))
                else:
                    start = (step*i)
                    end = len(test_ids)

                # create array of indices
                indices = list(range(start, end))
                indices = np.array(indices)

                # generate batch of test data
                image_test_batch = generate_batch(indices,*args,train_set=False)
                # feed dict for test batch
                feed_dict_test =  {self.X: image_test_batch.astype(np.float32),
                                   self.training: False,
                                   self.cond: np.array([0,0])}
                # compute predicted masks
                mask_predicted = sess.run(self.Y, feed_dict=feed_dict_test)
                # resize mask
                if mask_predicted.shape[1] == HEIGHT and mask_predicted.shape[2] == WIDTH:
                    mask_predicted = resize_image(mask_predicted, HEIGHT, WIDTH)
                # binirize masks
                mask_predicted = binirize_mask(mask_predicted, threshold=threshold)
                # encode masks
                EncodedPixels = rle_encode_multimage(mask_predicted)

                # save the predictions to the designated csv file
                if i == 0:
                    submission = pd.DataFrame({"id":test_ids[indices],"rle_mask": EncodedPixels},
                                              columns=["id","rle_mask"])
                    submission.id = submission.id.fillna('')
                    submission.to_csv(submission_path, header = True, index=False)
                else:
                    submission = pd.DataFrame({"id":test_ids[indices],"rle_mask": EncodedPixels},
                                              columns=["id","rle_mask"])
                    submission.id = submission.id.fillna('')
                    submission.to_csv(submission_path, mode='a', header = False, index=False)

                # update the progress bar
                bar.update(i+1, number_of_folds_completed = i+1)

            # finish the progress bar
            bar.finish()
