
from setuptools import setup

setup(name='models',
      version='0.1.0',
      description='Improved U-Net',
      long_description=open('README.md').read(),
      url='https://gitlab.com/itratrahman/improved_unet',
      author='Itrat Rahman',
      author_email='rahmanitrat@outlook.com',
      license='MIT License',
      packages=['models'],
      install_requires=["numpy", "pandas", "scipy", "scikit-image","progressbar", "tensorflow-gpu"],
      zip_safe=False)
